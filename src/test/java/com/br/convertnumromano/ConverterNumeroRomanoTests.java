package com.br.convertnumromano;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConverterNumeroRomanoTests {
    @Test
    public void testarConverterNumeroRomano987ParaCMLXXXVII(){
        Assertions.assertEquals(Romano.coversorDeNumero(987),"CMLXXXVII");
    }
    @Test
    public void testarConverterNumeroRomano10paraX(){
        Assertions.assertEquals(Romano.coversorDeNumero(10),"X");
    }
    @Test
    public void testarConverterNumeroRomano5paraV(){
        Assertions.assertEquals(Romano.coversorDeNumero(5),"V");
    }
    @Test
    public void testarConverterNumeroRomano50paraC(){
        Assertions.assertEquals(Romano.coversorDeNumero(50),"L");
    }

    @Test
    public void testarConverterNumeroRomanoMaiorMaximo(){
         try {
            Assertions.assertEquals("numero Maximo é 1000", Romano.coversorDeNumero(1001));
        }
        catch (Exception e) {
            String expectedMessage = "numero Maximo é 1000";
            Assertions.assertEquals( "numero Maximo é 1000", expectedMessage, e.getMessage() );
        }
    }

}
